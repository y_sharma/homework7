
import java.util.*;

// tegin mitmed meetodid nende j�rgi:
// https://www.youtube.com/watch?v=ceECSn0W3pE
// http://rosettacode.org/wiki/Huffman_coding#Java
// https://bitbucket.org/4lizs/homework7

/**
 * Prefix codes and Huffman tree. Tree depends on source data.
 */
public class Huffman {

	// kodeeritud andmete pikkus bittides
	int bitLength;
	// baidi esinemise sageduse tabel
	HashMap<Byte, Integer> frequency;
	// baidi ja selle koodi tabel
	HashMap<Byte, String> encode;

	// Huffman-i puu tippude klass
	private class HuffmanTreeNode implements Comparable<HuffmanTreeNode> {

		// esinemise sagedus
		int occurence;
		byte value;
		HuffmanTreeNode left;
		HuffmanTreeNode right;

		/*// esimene (j�rgmine) alluv
		HuffmanTreeNode firstChild;
		// parem (j�rgmine) naaber
		HuffmanTreeNode nextSibling;*/

		// konstruktor
		public HuffmanTreeNode() {
		}

		// konstruktor
		public HuffmanTreeNode(byte value, int occurence, HuffmanTreeNode left, HuffmanTreeNode right) {
			this.value = value;
			this.occurence = occurence;
			this.left = left;
			this.right = right;
		}

		// kui tipp on alluvateta, tagastab true
		public boolean isLeaf() {
			// return (firstChild == null);
			return (left == null && right == null);
		}

		@Override
		// sorteetib tipub sageduse j�rgi
		public int compareTo(HuffmanTreeNode o) {
			// sorteerib kasvavalt
			return occurence - o.occurence;
		}

	}

	/**
	 * Constructor to build the Huffman code for a given bytearray.
	 * 
	 * @param original
	 *            source data
	 */
	Huffman(byte[] original) {
		if (original == null) {
			throw new RuntimeException("Sisend on null");
		}
	}

	/**
	 * Length of encoded data in bits.
	 * 
	 * @return number of bits
	 */
	public int bitLength() {
		return bitLength;
	}

	/**
	 * Encoding the byte array using this prefixcode.
	 * 
	 * @param origData
	 *            original data
	 * @return encoded data
	 */
	// ehk siis sisse tuleb baitmassiiv origData
	// v�ljastada tuleb kodeeritud baitmassiiv
	public byte[] encode(byte[] origData) {
		// loendab korduvate andmete esinemise sageduse
		this.frequency = countFrequency(origData);
		// teeb puu
		HuffmanTreeNode root = generateTree(frequency);

		this.encode = getEncode(root);
		
		StringBuffer sb = new StringBuffer();
		for (byte b : origData) {
			sb.append(encode.get(b));
		}

		bitLength = sb.length();

		List<Byte> bytes = new ArrayList<Byte>();
		while (sb.length() > 0) {
			while (sb.length() < 8)
				sb.append('0');
			String str = sb.substring(0, 8);
			bytes.add((byte) Integer.parseInt(str, 2));
			sb.delete(0, 8);
		}
		byte[] ret = new byte[bytes.size()];
		for (int i = 0; i < bytes.size(); i++) {

			ret[i] = bytes.get(i);
		}
		return ret;

		/*
				// leiab unikaalsete andmete listi
				List<Byte> uniqData = uniqData(origData);
		
				// unikaalsete andmete arv
				int countOfUniqData = uniqData.size();
		
				// teeb massiivi, mille suuruseks on unikaalsete andmete arv
				int[] countOfUniqData = new int[uniqData.size()];
				
				// t�idab massiivi esialgu nullidega
				for (int i = 0; i < countOfUniqData.length; i++) {
					countOfUniqData[i] = 0;
				}
				
				// loendab, mitu korda antud andmed esinevad (korduvad)
				// ja lisab tlemused massiivi
				for (int i = 0; i < uniqData.size(); i++) {
					byte checker = uniqData.get(i);
					for (int j = 0; j < origData.length; j++) {
						if (checker == origData[j]) {
							countOfUniqData[i]++;
						}
					}
				}
				
				// sorteerib massiivi
				Arrays.sort(countOfUniqData);
		
				// puu massiiv, suuruseks on unikaalsete andmete arv
				HuffmanTreeNode[] tree = new HuffmanTreeNode[countOfUniqData];
		
				// leiab alluvateta tipud (lehed) ja lisab need puu massiivi
				for (int i = 0; i < countOfUniqData; i++) {
					byte nodeCode = uniqData.get(i);
					String nodeName = Character.toString((char) nodeCode);
					int nodeOccurance = countOccurance(origData, nodeName);
					tree[i] = new HuffmanTreeNode(nodeName, nodeCode, nodeOccurance);
				}*/

	}

	// baidi ja selle stringi tabel
	private HashMap<Byte, String> getEncode(HuffmanTreeNode root) {
		HashMap<Byte, String> result = new HashMap<Byte, String>();
		codeLeaf(root, "", result);

		return result;
	}

	// lisab lehtedele andmed
	private void codeLeaf(HuffmanTreeNode node, String code, HashMap<Byte, String> encode) {
		if (node.isLeaf()) {
			code = (code.length() > 0) ? code.toString() : "0";
			encode.put(node.value, code);
		} else {
			if (node.left != null)
				codeLeaf(node.left, code + "0", encode);
			if (node.right != null)
				codeLeaf(node.right, code + "1", encode);
		}

	}

	// teeb valmis puu
	private HuffmanTreeNode generateTree(HashMap<Byte, Integer> frequency) {
		PriorityQueue<HuffmanTreeNode> tree = new PriorityQueue<HuffmanTreeNode>();
		for (byte key : frequency.keySet())
			tree.add(new HuffmanTreeNode(key, frequency.get(key), null, null));

		while (tree.size() > 1) {
			HuffmanTreeNode left = tree.poll();
			HuffmanTreeNode right = tree.poll();
			// TODO m�rk
			tree.add(new HuffmanTreeNode((byte) '#', left.occurence + right.occurence, left, right));
		}

		return tree.poll();
	}

	// loendab korduvate andmete esinemise sageduse
	private HashMap<Byte, Integer> countFrequency(byte[] origData) {
		HashMap<Byte, Integer> result = new HashMap<Byte, Integer>();

		for (int i = 0; i < origData.length; i++) {
			byte key = origData[i];
			if (result.containsKey(key))
				result.put(key, result.get(key) + 1);
			else
				result.put(key, 1);
		}
		return result;
	}

	/*	// loeb kokku, mitu korda etteantud string baitmassiivis esineb
		private int countOccurance(byte[] origData, String givenString) {
			int result = 0;
			for (int i = 0; i < origData.length; i++) {
				if (origData[i] == givenString.charAt(0)) {
					result++;
				}
			}
			return result;
	
		}
	
		// tagastab listi unikaalsetest andmetest baitmassiivis (korduvaid ei j�ta
		// alles)
		private List<Byte> uniqData(byte[] origData) {
			List<Byte> result = new ArrayList<Byte>();
	
			for (int i = 0; i < origData.length; i++) {
				// kui selliseid andmeid ei ole tulemuse listis
				if (!(result.contains(origData[i]))) {
					// siis lisada see listi juurde
					result.add(origData[i]);
				}
			}
			return result;
		}*/

	/**
	 * Decoding the byte array using this prefixcode.
	 * 
	 * @param encodedData
	 *            encoded data
	 * @return decoded data (hopefully identical to original)
	 */
	public byte[] decode(byte[] encodedData) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < encodedData.length; i++) {

			sb.append(String.format("%8s", Integer.toBinaryString(encodedData[i] & 0xFF)).replace(' ', '0'));
		}
		String str = sb.substring(0, bitLength);
		List<Byte> bytes = new ArrayList<Byte>();
		String code = "";
		while (str.length() > 0) {
			code += str.substring(0, 1);
			str = str.substring(1);
			Iterator<Byte> list = encode.keySet().iterator();
			while (list.hasNext()) {
				Byte leaf = list.next();
				if (encode.get(leaf).equals(code)) {
					bytes.add(leaf);
					code = "";
					break;
				}
			}
		}
		byte[] ret = new byte[bytes.size()];
		for (int i = 0; i < bytes.size(); i++) {
			ret[i] = bytes.get(i);
		}
		return ret;

	}

	/** Main method. */
	public static void main(String[] params) {
		String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
		byte[] orig = tekst.getBytes();
		Huffman huf = new Huffman(orig);
		byte[] kood = huf.encode(orig);
		byte[] orig2 = huf.decode(kood);
		// must be equal: orig, orig2
		System.out.println(Arrays.equals(orig, orig2));
		int lngth = huf.bitLength();
		System.out.println("Length of encoded data in bits: " + lngth);
	}
}
